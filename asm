section .data

extern sca, scb, scc, scRes, scNum, scDen, scPr
extern usa, usb, usc, usDen
extern usRes, usNum, usPr

global asm_func_sc
global asm_func_us

section .text 

asm_func_sc:
;вычисление числителя
mov al,-74
cbw
mov bl, [sca]
idiv bl
mov bl, [scc]
add al, bl
sub al, 5
mov [scNum], al
;вычисление знаменателя
mov al, [scc]
cbw
mov bl, [scb]
imul bl
mov bl, 2
idiv bl
inc al
mov [scDen], al
;вычисление всего выражения
mov al, [scNum]
cbw
mov bl, [scDen]
idiv bl
mov [scRes], al
ret

asm_func_us:
;вычисление числителя
mov ax,-74
cwd
mov bx, [usa]
idiv bx
mov bx, [usc]
add ax, bx
sub ax, 5
mov [usNum], ax
;вычисление знаменателя
mov ax, [usc]
cwd
mov bx, [usb]
mul bx
mov bx, 2
div bx
inc ax
mov [usDen], ax
;вычисление всего выражения
mov ax, [usNum]
cwd
mov bx, [usDen]
idiv bx
mov [usRes], ax
ret
